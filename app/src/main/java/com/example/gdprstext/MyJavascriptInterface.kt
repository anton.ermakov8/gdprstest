package com.example.gdprstext

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.webkit.JavascriptInterface
import androidx.core.content.edit
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

open class MyJavascriptInterface internal constructor(var activity: Activity?) {

    @JavascriptInterface
    open fun acceptGDPR() {
        activity?.let {
            it.getSharedPreferences("local", Context.MODE_PRIVATE).edit {
                putBoolean("first_launch", false)
                putBoolean("gdprWasOpened", true)
            }

            val intent = Intent(it, MainActivity::class.java)
            it.startActivity(intent)
            it.finish()
        }
    }

    @JavascriptInterface
    open fun declineGDPR() {
        (activity as? MainActivity)?.let {
            Snackbar.make(it.web, R.string.decline_gdpr, Snackbar.LENGTH_LONG).show()
        }

    }

}